function randomIntFromInterval(min, max) {
  // min and max included
  return Math.floor(Math.random() * (max - min + 1) + min);
}

function getRandomData(category) {
  const api = [{ name: '- Select ' + category + ' - ', value: '' }];
  const randNum = randomIntFromInterval(3, 8);

  for (let i = 1; i <= randNum; i++) {
    api.push({ name: category + '-' + i, value: category + '-' + i });
  }
  return api;
}

module.exports = {
  getRandomData,
  randomIntFromInterval,
};
