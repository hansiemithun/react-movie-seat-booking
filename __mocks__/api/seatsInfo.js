const { randomIntFromInterval } = require('./utility');

let api = {
  maxSeatsInRow: 20,
  rows: [
    { name: 'A', totalSeats: 14, price: 150 },
    { name: 'B', totalSeats: 15, price: 150 },
    { name: 'C', totalSeats: 15, price: 150 },
    { name: '', category: 'Classic: Rs 150.00' },
    { name: 'D', totalSeats: 15, price: 230 },
    { name: 'E', totalSeats: 15, price: 230 },
    { name: 'F', totalSeats: 16, price: 230 },
    { name: 'G', totalSeats: 16, price: 230 },
    { name: 'H', totalSeats: 16, price: 230 },
    { name: 'I', totalSeats: 16, price: 230 },
    {
      name: 'J',
      totalSeats: 19,
      splitter: 17,
      price: 230,
    },
    { name: 'K', totalSeats: 19, splitter: 17, price: 230 },
    { name: 'L', totalSeats: 20, splitter: 17, price: 230 },
    { name: 'M', totalSeats: 20, splitter: 17, price: 230 },
    { name: '', category: 'Prime: Rs 230.00' },
  ],
};

function getRandomBookedSeats(row) {
  const randomBookedSeats = [];
  for (let i = 1; i < row.totalSeats; i++) {
    const toss = randomIntFromInterval(i, 100) % 2 === 0;
    if (toss) {
      randomBookedSeats.push(i);
    }
  }
  return randomBookedSeats;
}

function getRandomBlankSeat(totalSeats) {
  const randomBlank = [];
  const rand = randomIntFromInterval(1, totalSeats - 2);
  for (let i = 1; i <= totalSeats; i++) {
    if (i !== rand && i + 1 !== rand) {
      randomBlank.push(i);
    }
  }
  return randomBlank;
}

module.exports = function(req, res) {
  let seatCapacity = 0;
  let totalSeatsBooked = 0;
  const compareParams = { movie: 'Movie-1', theatre: 'Theatre-1', timings: '11:00 AM' };
  let codeCheck = JSON.stringify(compareParams) === JSON.stringify(req.query);

  if (codeCheck) {
    const randomRow = randomIntFromInterval(0, api.rows.length - 1);
    api.rows.map(function(row, i) {
      if (row.totalSeats) {
        if (i !== randomRow) {
          row.completeRowBooked = true;
          totalSeatsBooked += row.totalSeats;
        } else {
          const randomBookedSeats = getRandomBlankSeat(row.totalSeats);
          row.bookedSeats = randomBookedSeats;
          row.completeRowBooked = false;
          totalSeatsBooked += randomBookedSeats.length;
        }
        seatCapacity += row.totalSeats;
      }
    });
  } else {
    api.rows.map(function(row, i) {
      if (row.totalSeats) {
        const toss = randomIntFromInterval(i, 100) % 2 === 0;
        if (toss) {
          const tossForFull = randomIntFromInterval(i, 100) % 2 === 0;
          if (tossForFull) {
            row.completeRowBooked = true;
            totalSeatsBooked += row.totalSeats;
          }
        } else {
          const tossForEmpty = randomIntFromInterval(i, 100) % 2 === 0;
          if (tossForEmpty) {
            const randomBookedSeats = getRandomBookedSeats(row);
            row.bookedSeats = randomBookedSeats;
            totalSeatsBooked += randomBookedSeats.length;
          }
        }
        seatCapacity += row.totalSeats;
      }
    });
  }

  api.seatCapacity = seatCapacity;
  api.totalSeatsBooked = totalSeatsBooked;
  const { movie, theatre, numberOfSeats } = req.query;
  api.movie = movie;
  api.theatre = theatre;
  api.numberOfSeats = numberOfSeats;
  res.json({ seatsInfo: api });
};
