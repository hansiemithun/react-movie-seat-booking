module.exports = function(req, res) {
  const numberOfSeats = [];
  const maxNumber = req.query.numberOfSeats || 6;
  numberOfSeats.push({ name: ' - ', value: '' });
  for (let i = 1; i <= maxNumber; i++) {
    numberOfSeats.push({ name: i, value: i });
  }
  res.json({ numberOfSeats });
};
