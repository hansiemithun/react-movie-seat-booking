/* eslint-disable-next-line no-unused-vars */
const { getRandomData } = require('./utility');

module.exports = function(req, res) {
  res.json({
    movies: getRandomData('Movie'),
  });
};
