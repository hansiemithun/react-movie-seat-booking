const { getRandomData } = require('./utility');

module.exports = function(req, res) {
  const { movie } = req.query;
  res.json({ theatres: getRandomData('Theatre', req), movie });
};
