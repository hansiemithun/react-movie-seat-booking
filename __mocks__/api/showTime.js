const { randomIntFromInterval } = require('./utility');

const timings = ['11:00 AM', '02:00 PM', '04:30 PM', '06:45 PM', '8:00 PM', '10:00 PM'];

module.exports = function(req, res) {
  const randTiming = randomIntFromInterval(1, timings.length - 1);
  res.json({ showTime: timings.slice(0, randTiming) });
};
