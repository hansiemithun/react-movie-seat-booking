/* eslint-disable-next-line no-unused-vars */
const { getRandomData } = require('./theatres');

const api = {
  seatList: {
    maxSeatsInRow: 20,
    selectedNumberOfSeats: 3,
    rows: [
      { name: 'A', totalSeats: 14, price: 150, completeRowBooked: true },
      { name: 'B', totalSeats: 15, price: 150, completeRowBooked: true },
      { name: 'C', totalSeats: 15, price: 150, completeRowBooked: true },
      { name: '', category: 'Classic: Rs 150.00' },
      { name: 'D', totalSeats: 15, price: 230, completeRowBooked: true },
      { name: 'E', totalSeats: 15, price: 230, completeRowBooked: true },
      { name: 'F', totalSeats: 16, price: 230, completeRowBooked: true },
      { name: 'G', totalSeats: 16, price: 230, completeRowBooked: true },
      { name: 'H', totalSeats: 16, price: 230, completeRowBooked: true },
      { name: 'I', totalSeats: 16, price: 230, completeRowBooked: true },
      {
        name: 'J',
        totalSeats: 19,
        splitter: 17,
        price: 230,
        completeRowBooked: true,
      },
      { name: 'K', totalSeats: 19, splitter: 17, price: 230, completeRowBooked: true },
      { name: 'L', totalSeats: 20, splitter: 17, price: 230, completeRowBooked: true },
      { name: 'M', totalSeats: 20, splitter: 17, price: 230, completeRowBooked: true },
      { name: '', category: 'Prime: Rs 230.00' },
    ],
  },
  showTimings: ['11:00 AM', '02:00 PM', '04:30 PM', '06:45 PM', '8:00 PM', '10:00 PM'],
  maxSeatsSelection: [
    { name: 1, value: 1 },
    { name: 2, value: 2 },
    { name: 3, value: 3 },
    { name: 4, value: 4 },
    { name: 5, value: 5 },
    { name: 6, value: 6 },
  ],
  movieList: getRandomData('Movie'),
  theatreList: [{ name: '- Select Theatre - ', value: '' }],
};

module.exports = function(req, res) {
  res.json(api);
};
