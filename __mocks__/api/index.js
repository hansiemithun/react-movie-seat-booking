const homeApi = require('./home');
const moviesApi = require('./movies');
const theatresApi = require('./theatres');
const showTimeApi = require('./showTime');
const seatsInfoApi = require('./seatsInfo');
const numberOfSeatsApi = require('./numberOfSeats');

module.exports = {
  homeApi,
  moviesApi,
  theatresApi,
  showTimeApi,
  seatsInfoApi,
  numberOfSeatsApi,
};
