This project was bootstrapped using [Ozone UI Generator](https://npmjs.com/package/generator-ozone-ui).

## Seat Reservation System

### Run Command

After cloning, run the below commands inside your project folder:

### `npm install`

### `npm start`

Then, App runs at: (http://localhost:3000) pointing to mockserver: (http://localhost:4001)

### Demo:

![Seat Reservation System](http://g.recordit.co/fXoEUprjGR.gif 'Seat Reservation System')

### Scenario Demo

Select

- **Movie:** Movie-1
- **Theatre:** Theatre-1
- **Timing:** 11:am
- ** Select Seats: ** > 2

All seats are booked except 2 seats for the above scenario. If user selects 2 available seats and click on un-available seats 5 times consecutively, he will get an error as:

#### Sorry! Seats are occupied. Try a different time/date.
