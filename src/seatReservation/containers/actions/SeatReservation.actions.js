import axios from 'axios';
import envConfig from '../../../core/env-config.json';

const environment = process.env.REACT_APP_ENVIRONMENT;
const apiUrl = envConfig[environment]['api_url'];

export const actionTypes = {
  FETCH_MOVIES: 'FETCH_MOVIES',
  FETCH_THEATRES: 'FETCH_THEATRES',
  FETCH_TIMINGS: 'FETCH_TIMINGS',
};

async function fetchAPI(url) {
  const response = await axios(url);
  return response.data;
}

export const fetchMovies = () => {
  console.log('fetchMovies called');
  // const movieResponse = fetchAPI(apiUrl);
  // console.log('movieResponse => ', JSON.stringify(movieResponse));
  // dispatch({ type: actionTypes.FETCH_MOVIES, payload: [] });
};

export const theatresAction = () => ({ type: actionTypes.FETCH_THEATRES });
export const timingsAction = () => ({ type: actionTypes.FETCH_TIMINGS });
