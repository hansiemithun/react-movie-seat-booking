import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { fetchMovies } from './actions/SeatReservation.actions';
import { initialState } from './reducer/SeatReservation.reducer';

export class SeatReservationContainer extends Component {
  componentDidMount() {
    this.props.dispatch(fetchMovies());
  }

  render() {
    console.log('movies => ', JSON.stringify(this.props.movies));
    return <h1>Hello!</h1>;
  }
}

const mapStateToProps = state => {
  return {
    movies: state.movies,
  };
};

// function mapDispatchToProps(dispatch) {
//   return {
//     dispatch,
//     ...bindActionCreators({ fetchMovies }, dispatch),
//   };
// }

function mapDispatchToProps(dispatch) {
  return { dispatch, fetchMovies: bindActionCreators(fetchMovies, dispatch) };
}

// function mapDispatchToProps(dispatch) {
//   return {
//     fetchMovies: bindActionCreators(fetchMovies, dispatch),
//   };
// }

SeatReservationContainer.defaultProps = initialState;

SeatReservationContainer.propTypes = {
  movies: PropTypes.array.isRequired,
  fetchMovies: PropTypes.func.isRequired,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SeatReservationContainer);
