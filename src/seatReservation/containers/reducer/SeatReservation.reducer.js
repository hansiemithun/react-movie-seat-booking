import { actionTypes } from '../actions/SeatReservation.actions';
const { FETCH_MOVIES, FETCH_THEATRES, FETCH_TIMINGS } = actionTypes;

export const initialState = {
  movies: [{ name: '- Select Movie -', value: '' }],
  theatres: [{ name: '- Select Theatre -', value: '' }],
  timings: ['00:00', '00:00', '00:00', '00:00', '00:00', '00:00'],
  numberOfSeats: [],
  seats: [],
  legend: [],
};

export default (state = initialState, { type, payload }) => {
  const newState = { ...state };
  switch (type) {
    case FETCH_MOVIES:
      return { ...state, movies: [newState.movies, payload] };
    case FETCH_THEATRES:
      return { ...state, theatres: [newState.theatres, payload] };
    case FETCH_TIMINGS:
      return { ...state, timings: [newState.timings, payload] };
    case 'FETCH_NUMBER_OF_SEATS':
      return { ...state, numberOfSeats: [newState.numberOfSeats, payload] };
    case 'FETCH_SEATS':
      return { ...state, seats: [newState.seats, payload] };
    case 'FETCH_LEGEND':
      return { ...state, legend: [newState.legend, payload] };
    default:
      return newState;
  }
};
