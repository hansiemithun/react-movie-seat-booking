import React, { Component } from 'react';
import $ from 'jquery';
import { Provider } from 'react-redux';
import store from './reduxStore/redux.store';
// import { SeatReservationContainer } from './seatReservation/containers/SeatReservation.container';
import Main from './Main';

window.jQuery = $;
window.$ = $;
global.jQuery = $;

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Main />
      </Provider>
    );
  }
}

export default App;
