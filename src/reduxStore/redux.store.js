import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import seatReservationReducer from '../seatReservation/containers/reducer/SeatReservation.reducer';

const middlewares = [thunk];

const store = createStore(
  seatReservationReducer,
  composeWithDevTools(applyMiddleware(...middlewares))
);

export default store;
