const home = {
  url: '/',
};

const movies = {
  url: '/movies',
};

const theatres = {
  url: '/theatres',
};

const showTime = {
  url: '/showTime',
};

const numberOfSeats = {
  url: '/numberOfSeats',
};

const seatsInfo = {
  url: '/seatsInfo',
};

module.exports = {
  home,
  movies,
  theatres,
  showTime,
  seatsInfo,
  numberOfSeats,
};
