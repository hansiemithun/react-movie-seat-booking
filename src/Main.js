import React, { useState, useEffect } from 'react';
import axios from 'axios';
import $ from 'jquery';
import { Container, Form, Row, Col, Button } from 'react-bootstrap';
import Dropdown from './widgets/dropDown/DropDown';
import SeatLayout from './components/seatLayout/SeatLayout';
import ShowTiming from './components/showTimings/ShowTimings';
import Legend from './components/legend/Legend';
import envConfig from './core/env-config.json';
import { movies, theatres, showTime, seatsInfo, numberOfSeats } from './common/restApiConstants';
import { initState } from './utility';
import 'bootstrap/dist/css/bootstrap.css';
import './App.scss';

window.jQuery = $;
window.$ = $;
global.jQuery = $;

const environment = process.env.REACT_APP_ENVIRONMENT;
const apiUrl = envConfig[environment]['api_url'];

const selected = {
  movie: '',
  theatre: '',
  timings: '',
  numberOfSeats: '',
  seatsBooked: {},
};

function Main() {
  const [movieList, setMovieList] = useState(initState.movieList);
  const [theatreList, setTheatreList] = useState(initState.theatreList);
  const [showTimingsList, setShowTimingsList] = useState(initState.showTimingsList);
  const [seatList, setSeatList] = useState(initState.seatsInfo);
  const [numberOfSeatsList, setNumberOfSeatsList] = useState(initState.numberOfSeatsList);
  const [selectedItems, setSelectedItems] = useState(selected);

  async function fetchAPI(url, callback = '') {
    const response = await axios(url);
    if (callback) callback(response.data);
  }

  useEffect(() => {
    fetchAPI(apiUrl + movies.url, function(data) {
      setMovieList(data.movies);
    });
  }, []);

  const movieListConfig = {
    identifier: 'movieList',
    options: movieList,
    handleChange: function(el) {
      setSelectedItems({ ...selected, movie: el.target.value });
      fetchAPI(apiUrl + theatres.url + '?movie=' + el.target.value, function(data) {
        setTheatreList(data.theatres);
      });
    },
  };

  const seatListConfig = {
    seatsInfo: seatList,
    bookedSeatsCB: seatsBooked => {
      const newState = { ...selectedItems, seatsBooked };
      console.log('seatsBooked => ', seatsBooked, ' && newState => ', JSON.stringify(newState));
      setSelectedItems(newState);
    },
  };

  const theatreListConfig = {
    identifier: 'theatreList',
    options: theatreList,
    handleChange: function(el) {
      setSelectedItems({ ...selectedItems, theatre: el.target.value, timings: '' });
      const showTimeUrl =
        apiUrl + showTime.url + '?movie=' + selectedItems.movie + '&theatre=' + el.target.value;
      fetchAPI(showTimeUrl, function(data) {
        setShowTimingsList(data.showTime);
      });
    },
  };

  const numberOfSeatsListConfig = {
    identifier: 'seatsList',
    heading: 'Select number of seats',
    options: numberOfSeatsList,
    handleChange: function(el) {
      const seatsValue = el.target.value;
      setSelectedItems({ ...selectedItems, numberOfSeats: seatsValue });
      if (seatsValue !== selected.numberOfSeats) {
        checkSeatsAvailability(seatsValue);
      }
    },
  };

  const showTimingsListConfig = {
    timings: showTimingsList,
    handleCallback: function(value) {
      setSelectedItems({ ...selectedItems, timings: value });
      const { movie, theatre } = selectedItems;
      const queryParams = `?movie=${movie}&theatre=${theatre}&timings=${value}`;
      const numberOfSeatsUrl = `${apiUrl + numberOfSeats.url + encodeURI(queryParams)}`;
      fetchAPI(numberOfSeatsUrl, function(data) {
        setNumberOfSeatsList(data.numberOfSeats);
      });
    },
  };

  const checkSeatsAvailability = seatsToBook => {
    const { movie, theatre, timings } = selectedItems;
    const queryParams = `?movie=${movie}&theatre=${theatre}&timings=${timings}&numberOfSeats=${seatsToBook}`;
    const bookingUrl = `${apiUrl + seatsInfo.url + encodeURI(queryParams)}`;
    fetchAPI(bookingUrl, function(data) {
      setSeatList(data.seatsInfo);
    });
  };

  const validateConfirm = () => {
    const valid =
      selectedItems.movie !== selected.movie &&
      selectedItems.theatre !== selected.theatre &&
      selectedItems.timings !== selected.timings &&
      selectedItems.numberOfSeats !== selected.numberOfSeats &&
      selectedItems.seatsBooked !== selected.seatsBooked &&
      Object.keys(selectedItems.seatsBooked).length === parseInt(selectedItems.numberOfSeats);
    return !valid;
  };

  const confirmTickets = () => {
    const tickets = JSON.stringify(selectedItems.seatsBooked);
    if (window.confirm(`Are you sure to book this tickets: ${tickets}?`)) {
      alert(`Congrats! tickets confirmed: ', ${tickets}`);
      window.location.reload();
    }
  };

  return (
    <Container className="wrapper">
      <Row>
        <Col className="container border-right" lg={3} md={3}>
          <Form>
            <div className="region-wrapper">
              <Dropdown config={movieListConfig} />
              <Dropdown config={theatreListConfig} />
              <div className="line" />
              <ShowTiming config={showTimingsListConfig} />
              <div className="line" />
              <Dropdown config={numberOfSeatsListConfig} />
              <Button variant="success" disabled={validateConfirm()} onClick={confirmTickets}>
                Confirm
              </Button>
            </div>
          </Form>
        </Col>
        <Col className="container border-right" lg={6} md={6}>
          <div className="region-wrapper">
            <SeatLayout config={seatListConfig} />
          </div>
        </Col>
        <Col lg={3} md={3}>
          <div className="region-wrapper">
            <Legend />
            <div className="line" />
            <div className="capacity">
              <span>Seats capacity: </span>
              {seatList.seatCapacity || 0} <br />
              <span>Booked seats: </span>
              {seatList.totalSeatsBooked || 0} <br />
              <span> Available seats: </span>
              {seatList.seatCapacity - seatList.totalSeatsBooked || 0}
            </div>
          </div>
        </Col>
      </Row>
    </Container>
  );
}

export default Main;
