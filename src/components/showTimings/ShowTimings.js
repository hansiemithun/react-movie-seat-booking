import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { Form, Button } from 'react-bootstrap';
import './showTimings.scss';

function ShowTimings(props) {
  const initialRender = useRef(true);
  const [selected, setSelected] = useState(null);
  const { config } = props;
  const { timings, handleCallback } = config;

  /* 
   Equivalent to componentWillReceiveProps
   Reset selected state on props change 
  */
  useEffect(() => {
    if (!initialRender.timings) {
      setSelected(null);
    }
  }, [timings]);

  return (
    <div className="timings-wrapper">
      <Form.Group controlId="timings">
        <Form.Label>Select Timings</Form.Label>
        <div className="timings">
          <ul className="no-list-style">
            {renderList(timings, handleCallback, selected, setSelected)}
          </ul>
        </div>
      </Form.Group>
    </div>
  );
}

function renderList(timings, handleCallback, selected, setSelected) {
  const list = [];
  // eslint-disable-next-line
  timings.map((time, index) => {
    list.push(
      <li key={index}>
        <Button
          active={selected === time}
          onClick={() => {
            setSelected(time);
            handleCallback(time);
          }}
          variant="outline-secondary"
          size="sm">
          {time}
        </Button>
      </li>
    );
  });
  return list;
}

ShowTimings.propTypes = {
  config: PropTypes.object.isRequired,
};

export default ShowTimings;
