import React, { useState } from 'react';
import PropTypes from 'prop-types';
import './seatLayout.scss';

function SeatLayout(props) {
  const [count, setCount] = useState(1);
  const [bookedSeats, setBookedSeats] = useState({});

  const { seatsInfo, bookedSeatsCB } = props.config;
  const { rows, numberOfSeats } = seatsInfo;

  const createSeater = () => {
    const totalRowSeats = [];
    for (let row = rows.length - 1; row >= 0; row--) {
      const seats = [];
      // current row
      const cr = rows[row];
      // For no premium category labels
      if (cr['name'] !== '') {
        for (let col = 1; col <= cr.totalSeats; col++) {
          const key = cr.name + '-' + row + '-' + col;
          // used to have a splitter between seats division
          if (cr.splitter && col === cr.splitter) {
            seats.push(<span key={key + '-splitter'} className="splitter" />);
          }
          if (
            (cr.bookedSeats && cr.bookedSeats.length && cr.bookedSeats.indexOf(col) !== -1) ||
            cr.completeRowBooked
          ) {
            seats.push(
              <span
                onClick={() => setCounter(count, setCount)}
                key={key + '-seat-unavailable'}
                className="seat unavailable"
              />
            );
          } else {
            const seatNumber = `${cr.name + col}`;
            const seatNumberClass = ['seat', 'available', seatNumber];
            if (bookedSeats[seatNumber]) {
              seatNumberClass.push(bookedSeats[seatNumber]);
            }
            seats.push(
              <span
                onClick={() => selectSeats(seatNumber)}
                key={key + '-seat-available'}
                className={seatNumberClass.join(' ')}
              />
            );
          }
        }
        // }
      } else {
        seats.push(<span key={row + '-category'}>{cr.category}</span>);
      }
      totalRowSeats.push(
        <div key={row + '-row'} className={'row ' + cr.name + '_row'}>
          <span className="rowname">{cr.name}</span>
          {seats}
        </div>
      );
    }
    return totalRowSeats;
  };

  const selectSeats = seatNumber => {
    const totalBookedSeats = Object.keys(bookedSeats).length;
    const bookedSeatsState = { ...bookedSeats };
    if (!bookedSeatsState[seatNumber]) {
      bookedSeatsState[seatNumber] = 'booked';
    } else {
      delete bookedSeatsState[seatNumber];
      setBookedSeats(bookedSeatsState);
      bookedSeatsCB(bookedSeatsState);
    }

    if (totalBookedSeats < numberOfSeats) {
      setBookedSeats(bookedSeatsState);
    }

    if (parseInt(totalBookedSeats + 1) === parseInt(numberOfSeats)) {
      bookedSeatsCB(bookedSeatsState);
    }
  };

  return (
    <div className="seatLayout">
      <div className="rows">{createSeater()}</div>
    </div>
  );
}

function setCounter(count, setCount) {
  setCount(count + 1);
  if (count === 5) {
    alert('Sorry! Seats are occupied. Try a different time/date. ');
    setCount(1);
  }
}

SeatLayout.propTypes = {
  config: PropTypes.object.isRequired,
};

export default SeatLayout;
