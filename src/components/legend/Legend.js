import React from 'react';

function Legend() {
  return (
    <div className="seats-legends">
      <ul className="no-list-style">
        <li>
          <span className="success legend" />
          Your Booked seats
        </li>
        <li>
          <span className="half-success legend" />
          Possible seat combinations
        </li>
        <li>
          <span className="available legend" />
          Available seats
        </li>
        <li>
          <span className="unavailable legend" />
          Unavailable seats
        </li>
      </ul>
    </div>
  );
}

export default Legend;
