import React from 'react';
import PropTypes from 'prop-types';
import { Form } from 'react-bootstrap';

function Dropdown(props) {
  const { config } = props;
  const { handleChange, options, identifier, heading } = config;
  const styleConfig = ['dropdown-wrapper', identifier];

  return (
    <div className={styleConfig.join(' ')}>
      <Form.Group controlId={identifier}>
        {heading && <Form.Label>{heading}</Form.Label>}
        <Form.Control as="select" onChange={handleChange}>
          {renderOptions(options)}
        </Form.Control>
      </Form.Group>
    </div>
  );
}

function renderOptions(options) {
  const data = [];
  // eslint-disable-next-line
  options.map(option => {
    data.push(
      <option key={option.value} value={option.value}>
        {option.name}
      </option>
    );
  });
  return data;
}

Dropdown.propTypes = {
  config: PropTypes.object.isRequired,
};

export default Dropdown;
